﻿using System.Collections.Generic;
using System.Linq;
using Glide;
using MonoBehaviourExtensions;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
  private List<AudioSource> _clips = new List<AudioSource>();

  private void Awake()
  {
    _clips = GetComponentsInChildren<AudioSource>().ToList();
  }

  public AudioSource Play(string sound, bool createNew = false)
  {
    var data = GetAudio(sound);
    if (data != null)
    {
      if (createNew)
      {
        data = PlayClipAtPoint(data, transform.position);
      }

      data.Play();
      return data;
    }

    Debug.LogWarning("No sound: " + sound);

    return null;
  }

  public void TweenVolume(AudioSource source, float start, float target,float duration = 1f)
  {
    source.volume = start;
    Tweener.I.Tween(source, new {volume = target}, duration).Ease(Ease.CubeIn);
  }

  public bool IsPlaying(string soud)
  {
    var s = GetAudio(soud);
    return s.isPlaying;
  }

  public AudioSource GetAudio(string sound)
  {
    var data = _clips.Find(p => p.gameObject.name == sound);
    return data;
  }

  private static AudioSource PlayClipAtPoint(AudioSource audioSource, Vector3 pos)
  {
    GameObject temp = new GameObject("AS_" + audioSource.gameObject.name);
    temp.transform.position = pos;

    AudioSource aSource = temp.AddComponent<AudioSource>();

    aSource.clip = audioSource.clip;
    aSource.outputAudioMixerGroup = audioSource.outputAudioMixerGroup;
    aSource.mute = audioSource.mute;
    aSource.bypassEffects = audioSource.bypassEffects;
    aSource.bypassListenerEffects = audioSource.bypassListenerEffects;
    aSource.bypassReverbZones = audioSource.bypassReverbZones;
    aSource.playOnAwake = audioSource.playOnAwake;
    aSource.loop = audioSource.loop;
    aSource.priority = audioSource.priority;
    aSource.volume = audioSource.volume;
    aSource.pitch = audioSource.pitch;
    aSource.panStereo = audioSource.panStereo;
    aSource.spatialBlend = audioSource.spatialBlend;
    aSource.reverbZoneMix = audioSource.reverbZoneMix;
    aSource.dopplerLevel = audioSource.dopplerLevel;
    aSource.rolloffMode = audioSource.rolloffMode;
    aSource.minDistance = audioSource.minDistance;
    aSource.spread = audioSource.spread;
    aSource.maxDistance = audioSource.maxDistance;

    Destroy(temp, aSource.clip.length);
    return aSource;
  }

  private static AudioManager _i;
  public static AudioManager I
  {
    get
    {
      if (_i == null)
      {
        _i = Resources.Load<AudioManager>("AudioManager").Create();
      }

      return _i;
    }
  }
}