﻿using System.Collections.Generic;
using GameObjectExtensions;
using UnityEngine;

public class TiledScrollableX : MonoBehaviour
{
    public float ItemWidth = 5;
    public float Speed;
    public int Tiling;

    public GameObject Prefab;

    private readonly List<GameObject> _tiles = new List<GameObject>();

    private float _currentOffset;

    void Start()
    {
        for (int i = 0; i < Tiling; i++)
        {
            GameObject tile = Prefab.Create();
            tile.transform.parent = transform;
            tile.transform.localPosition = new Vector3(i * ItemWidth, 0, 0);

            _tiles.Add(tile);
        }
    }

    void Update()
    {
        float offset = Speed * Time.deltaTime;

        _currentOffset += offset;

        if (_currentOffset > ItemWidth)
        {
            _currentOffset -= ItemWidth;

            for (int i = 0; i < _tiles.Count; i++)
            {
                var tile = _tiles[i];
                tile.transform.localPosition = new Vector3(i * ItemWidth + _currentOffset, 0, 0);
            }
        }
        else
            foreach (var tile in _tiles)
            {
                tile.transform.localPosition += new Vector3(offset, 0, 0);
            }
    }

    public void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0, 0, 0.5f);

        float offset = (Tiling*ItemWidth) * 10;
        Gizmos.DrawCube(transform.position + new Vector3(offset/2, 0, 0), new Vector3(offset, ItemWidth*2, 2));
    }
}