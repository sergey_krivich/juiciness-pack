﻿using UnityEngine;
using System.Collections;

public class SinAnimation : MonoBehaviour
{
    public float Speed = 1;
    public float Amount = 1;
    public Vector3 Direction = new Vector3(0, 1, 0);

	
	// Update is called once per frame
	void Update ()
	{
	    transform.localPosition += Direction*Mathf.Cos(Time.time*Speed)*Amount*Time.deltaTime;
	}
}
