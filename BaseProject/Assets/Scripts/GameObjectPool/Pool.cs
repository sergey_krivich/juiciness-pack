﻿using System.Collections.Generic;
using UnityEngine;

using System;
using MonoBehaviourExtensions;


/// <summary>
/// Repository of commonly used prefabs.
/// </summary>
[AddComponentMenu("Gameplay/GameObjectPool")]
public class Pool : MonoBehaviour
{
  private static Pool _i;

  public static Pool I
  {
    get
    {
      if (_i == null)
      {
        _i = Resources.Load<Pool>("Pool").Create();
      }

      return _i;
    }
  }

  [Serializable]
  public class ObjectPoolEntry
  {
    [SerializeField] public GameObject Prefab;
    [SerializeField] public int Count;
  }

  [SerializeField] private ObjectPoolEntry[] _entries;

  private List<GameObject>[] _cache;

  protected GameObject ContainerObject;

  void Awake()
  {
    _i = this;

    ContainerObject = new GameObject("ObjectPool");
    DontDestroyOnLoad(ContainerObject);

    _cache = new List<GameObject>[_entries.Length];

    for (int i = 0; i < _entries.Length; i++)
    {
      var objectPrefab = _entries[i];

      _cache[i] = new List<GameObject>();

      for (int n = 0; n < objectPrefab.Count; n++)
      {
        var newObj = Instantiate(objectPrefab.Prefab) as GameObject;
        newObj.name = objectPrefab.Prefab.name;

        Put(newObj);
      }
    }
  }

  public GameObject Get(string objectType)
  {
    for (int i = 0; i < _entries.Length; i++)
    {
      var prefab = _entries[i].Prefab;

      if (prefab.name != objectType)
      {
        continue;
      }

      if (_cache[i].Count > 0)
      {
        GameObject pooledObject = _cache[i][0];
        _cache[i].RemoveAt(0);

        pooledObject.transform.parent = null;
        pooledObject.SetActive(true);

        return pooledObject;
      }

      return Instantiate(_entries[i].Prefab);
    }

    Debug.LogError("Prefab not found: " + objectType);
    return null;
  }

  public void Put(GameObject obj)
  {
    for (int i = 0; i < _entries.Length; i++)
    {
      if (_entries[i].Prefab.name != obj.name)
        continue;

      obj.SetActive(false);

      obj.transform.parent = ContainerObject.transform;

      _cache[i].Add(obj);

      return;
    }
  }
}