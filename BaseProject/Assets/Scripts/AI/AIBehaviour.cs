﻿using UnityEngine;

public class AIBehaviour : MonoBehaviour
{
  protected AIController _controller;

  public void Init(AIController c)
  {
    _controller = c;
  }

  public void AIUpdate()
  {
    if (!_controller.Dead)
      DoUpdate();
  }

  public virtual Vector2 GetInputAxis()
  {
    return new Vector2();
  }

  public virtual bool PlayerCollision(PlayerController p)
  {
    return true;
  }

  public virtual void NoticedPlayer()
  {

  }

  protected virtual void DoUpdate()
  {

  }

  public virtual void TriggerEnter(Collider2D collider2D1)
  {


  }

  public virtual void Unhidden()
  {

  }

  public virtual void OnEaten()
  {
  }

  public virtual void TriggerExit(Collider2D collider2D1)
  {

  }

  public virtual int GetDirection()
  {
    return 0;
  }

  public virtual void OnCollisionWithGuy()
  {

  }

  public void OnDie()
  {
    _controller.Dead = true;
  }
}
