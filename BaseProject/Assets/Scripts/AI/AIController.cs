﻿using System;
using System.Linq;
using UnityEngine;

public class AIController : MonoBehaviour
{
  public string StartBehaviour;

  public float NoticeDistance = 1f;
  public CharacterController2D Character;

  private float _lookDirection = 1;

  public bool Dead;

  private AIBehaviour _current;
  private AIController[] _controllers;


  public GameObject Stars;

  void Start()
  {
    Character = GetComponent<CharacterController2D>();
    Character.TriggerEnter += CharacterOnTriggerEnter;
    Character.TriggerExit += CharacterOnTriggerExit;

    var b = GetComponents<AIBehaviour>();
    foreach (var aiBehaviour in b)
    {
      aiBehaviour.enabled = false;
      aiBehaviour.Init(this);
    }

    _current = GetComponent(StartBehaviour) as AIBehaviour;
    if (_current != null) { _current.enabled = true;}
    else
    {
      Debug.Log("CURRENT IS NULL " + StartBehaviour);
    }

    _controllers = FindObjectsOfType<AIController>();
  }

  private void CharacterOnTriggerExit(Collider2D collider2D)
  {
    _current.TriggerExit(collider2D);
  }

  private void CharacterOnTriggerEnter(Collider2D collider2D)
  {
    _current.TriggerEnter(collider2D);


    if (collider2D.CompareTag("Enemy"))
    {
      var other = collider2D.GetComponent<AIController>();
      if (other != null)
      {
        if (other.Dead && !other.UsedForStun)
        {
          Destroy(other.gameObject);
          _current.OnCollisionWithGuy();
        }
      }
    }
  }

  public bool UsedForStun;

  public void Set<T>() where T : AIBehaviour
  {
    if (_current != null)
    {
      _current.enabled = false;
    }

    var b = GetComponent<T>();
    b.enabled = true;

    _current = b;
  }

  void Update()
  {
    if (Dead)
    {
      Character.jump = false;
    }
      var input = _current.GetInputAxis();

      if (input.x != 0)
      {
        _lookDirection = input.x;
      }

      int direction = _current.GetDirection();
      if (direction != 0)
      {
        _lookDirection = direction;
      }

      Character.ChangeMovement(input);


      CheckPlayerNotice();

      _current.AIUpdate();
  }

  private void CheckPlayerNotice()
  {
    float playerSide = -Mathf.Sign(transform.position.x - PlayerController.I.transform.position.x);
    float distance = Vector2.Distance(transform.position, PlayerController.I.transform.position);


    if (Math.Abs(playerSide - _lookDirection) < 0.001f && distance < NoticeDistance &&
        PlayerController.I.transform.position.y < transform.position.y + 0.95f)
    {
      foreach (var aiController in _controllers.Where(p => p != null))
      {
        if (aiController._lookDirection == _lookDirection &&
            Vector2.Distance(transform.position, aiController.transform.position) <
            aiController.NoticeDistance)
        {
          aiController._current.NoticedPlayer();
        }
      }

      NoticePlayer();
    }
  }

  public void NoticePlayer()
  {
     _current.NoticedPlayer();
  }

  // bool eat
  public bool PlayerCollision(PlayerController p)
  {
    if (Dead)
    {
      return false;
    }

    return _current.PlayerCollision(p);
  }

  public void Die(Transform other)
  {
    _current.OnDie();
  }
}
