﻿using System;
using UnityEngine;
using System.Collections;
using Glide;
using MonoBehaviourExtensions;

public class ScreenFade : MonoBehaviour
{
    private Fader _faderPrefab;

    private static ScreenFade _i;
    public static ScreenFade I
    {
        get
        {
            if (_i == null)
            {
                var go = new GameObject("ScreenFade");
                DontDestroyOnLoad(go);

                _i = go.AddComponent<ScreenFade>();
                _i._faderPrefab = Resources.Load<Fader>("UI/Fader");
            }

            return _i;
        }
    }


    public void FadeIn(float duration = 0.5f, Action onCompleted = null)
    {
        var fader = _faderPrefab.Create();
        fader.SetFade(duration, Fader.FadeMode.In, Ease.CubeIn, onCompleted);

    }

    public void FadeOut(float duration = 0.5f, Action onCompleted = null)
    {
        var fader = _faderPrefab.Create();
        fader.SetFade(duration, Fader.FadeMode.Out, Ease.CubeOut, onCompleted);
    }
}
