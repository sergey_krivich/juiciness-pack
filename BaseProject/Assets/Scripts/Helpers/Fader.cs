﻿using System;
using UnityEngine;
using Glide;
using UnityEngine.UI;

public class Fader : MonoBehaviour
{
    [SerializeField]
    private Image _image;

    private FadeMode _mode;

    public void SetFade(float duration, FadeMode fadeMode, Func<float, float> ease, Action onCompleted)
    {
        float targetAlpha;

        if (fadeMode == FadeMode.In)
        {
            targetAlpha = 0f;
        }
        else
        {
            targetAlpha = 1f;
        }
        Color color = _image.color;
        color.a = 1f - targetAlpha;
        _image.color = color;

        color.a = targetAlpha;

        Action easeCompleted = () =>
        {
            if (fadeMode == FadeMode.In)
            {
                Destroy(gameObject);
            }
        };
        easeCompleted += onCompleted;

        Tweener.I.Tween(_image, new {color = color}, duration)
            .Ease(ease)
            .OnComplete(() =>
            {
                easeCompleted();
            });
    }

    public enum FadeMode
    {
        In,
        Out
    }
}
