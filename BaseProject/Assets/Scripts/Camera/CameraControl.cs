﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CameraControl : JamBehaviour
{
    public float Speed = 5;
    public float RotationSpeed = 5;
    public float TargetRotation;
    public Transform Target;

    private readonly List<RumbleData> _rumbles = new List<RumbleData>(16);
    private readonly List<RumbleData> _rumblesRotation = new List<RumbleData>(16);
    private readonly RumbleData _rumbleMain = new RumbleData();
    private readonly RumbleData _rumbleMainRotation = new RumbleData();

    private void Start()
    {
        I = this;
    }

    private void Update()
    {
        if (Target == null)
        {
            return;
        }

        Vector3 curPos = position;
        Vector3 targetPos = Target.position + new Vector3(0, 0, -10);

        position = Vector3.Lerp(curPos, targetPos, Speed*Time.deltaTime);
        LocalRotation2d = Mathf.LerpAngle(LocalRotation2d, TargetRotation, Time.deltaTime*RotationSpeed);

        UpdateRumbles();
    }


    public void Rumble(float seconds, float amount, int pushesCount)
    {
        _rumbleMain._rumble = true;
        _rumbleMain._rumbleTime = seconds;
        _rumbleMain._rumbleAmount = amount;
        _rumbleMain._onePushTime = seconds/pushesCount;
        _rumbleMain._rumbleAmountStep = 0;
    }

    public void Rumble(float seconds, float startAmount, float endAmount, int pushesCount)
    {
        _rumbleMain._rumble = true;
        _rumbleMain._rumbleTime = seconds;
        _rumbleMain._rumbleAmount = startAmount;
        _rumbleMain._onePushTime = seconds/pushesCount;
        _rumbleMain._rumbleAmountStep = (endAmount - startAmount)/pushesCount;
    }


    public void RumbleAdditive(float seconds, float amount, int pushesCount)
    {

        _rumbles.Add(new RumbleData
        {
            _rumble = true,
            _rumbleTime = seconds,
            _rumbleAmount = amount,
            _onePushTime = seconds / pushesCount,
            _rumbleAmountStep = 0,
            _temp = 0
        });

    }

    public void RumbleAdditive(float seconds, float startAmount, float endAmount, int pushesCount)
    {
        _rumbles.Add(new RumbleData
        {
            _rumble = true,
            _rumbleTime = seconds,
            _rumbleAmount = startAmount,
            _onePushTime = seconds/pushesCount,
            _rumbleAmountStep = (endAmount - startAmount)/pushesCount,
            _temp = 0
        });
    }


    public void RumbleRotation(float seconds, float startAmount, float endAmount, int pushesCount)
    {
        _rumbleMain._rumble = true;
        _rumbleMain._rumbleTime = seconds;
        _rumbleMain._rumbleAmount = startAmount;
        _rumbleMain._onePushTime = seconds / pushesCount;
        _rumbleMain._rumbleAmountStep = (endAmount - startAmount) / pushesCount;
    }

    public void RumbleRotationAdditive(float seconds, float startAmount, float endAmount, int pushesCount)
    {
        _rumblesRotation.Add(new RumbleData
        {
            _rumble = true,
            _rumbleTime = seconds,
            _rumbleAmount = startAmount,
            _onePushTime = seconds / pushesCount,
            _rumbleAmountStep = (endAmount - startAmount) / pushesCount,
            _temp = 0
        });
    }


    private void UpdateRumbles()
    {
        foreach (var rumble in _rumbles.ToList())
        {
            UpdateRumble(rumble);
        }

        foreach (var rumble in _rumblesRotation.ToList())
        {
            UpdateRotationRumble(rumble);
        }

        UpdateRumble(_rumbleMain);
        UpdateRumble(_rumbleMainRotation);

        _rumbles.RemoveAll(p => !p._rumble);
    }

    private void UpdateRumble(RumbleData rumble)
    {
        rumble._rumbleTime -= Time.deltaTime;

        if (rumble._rumbleTime >= 0)
        {
            rumble._temp += Time.deltaTime;
            if (rumble._temp >= rumble._onePushTime)
            {
                short diapazon;
                if (RandomTool.NextInt(0, 2) == 1)
                    diapazon = -1;
                else
                    diapazon = 1;

                Vector2 direction = new Vector2((float) RandomTool.NextDouble()*diapazon,
                    (float) RandomTool.NextDouble()*diapazon);
                direction.Normalize();

                rumble._rumbleAmount += rumble._rumbleAmountStep;
                direction *= (rumble._rumbleAmount);

                position += (Vector3) direction;

                rumble._temp -= rumble._onePushTime;
            }
        }
        else
        {
            rumble._rumble = false;
        }
    }

    private void UpdateRotationRumble(RumbleData rumble)
    {
        rumble._rumbleTime -= Time.deltaTime;

        if (rumble._rumbleTime >= 0)
        {
            rumble._temp += Time.deltaTime;
            if (rumble._temp >= rumble._onePushTime)
            {
                float direction = RandomTool.NextSign();


                rumble._rumbleAmount += rumble._rumbleAmountStep;
                direction *= (rumble._rumbleAmount);

                LocalRotation2d += direction;

                rumble._temp -= rumble._onePushTime;
            }
        }
        else
        {
            rumble._rumble = false;
        }
    }

    private class RumbleData
    {
        public bool _rumble;
        public double _rumbleTime;
        public float _rumbleAmount;
        public float _rumbleAmountStep;
        public double _onePushTime;
        public double _temp;
    }

    public static CameraControl I { get; private set; }
}
