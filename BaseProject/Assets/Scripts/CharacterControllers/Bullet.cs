﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
  public float Speed;

  void Start()
  {

  }

  void Update()
  {

  }

  private void OnTriggerEnter2D(Collider2D coll)
  {
    if (coll.gameObject.name != "Player")
    {
      Destroy();
    }
  }

  public void Fire(Vector2 direction)
  {
    GetComponent<Rigidbody2D>().velocity = direction.normalized*Speed;
  }

  public void Destroy()
  {
    Pool.I.Put(gameObject);
  }
}
