﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
  public static PlayerController I { get; private set; }

  public GameObject Graphics;

  private CharacterController2D _characterController;

  void Awake()
  {
    I = this;
  }

  void Start()
  {
    _characterController = GetComponent<CharacterController2D>();
  }

  void Update()
  {
    _characterController.jump = !Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.Space);
    if (_characterController.CanJump && _characterController.jump)
    {
      //AudioManager.I.Play("Explosion");
    }
    Vector2 input = UpdateInputAxis();
    _characterController.ChangeMovement(input);
  }

  private Vector2 UpdateInputAxis()
  {
    Vector2 input = new Vector2();
    var scale = Graphics.transform.localScale;

    if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
    {
      input.x = 1;
    }
    else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
    {
      input.x = -1;
    }
    else
    {
      input.x = 0;
    }

    if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
    {
      input.y = 1;
      _characterController.ClimbLadder();
    }
    else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
    {
      input.y = -1;

      if (Input.GetKey(KeyCode.Space))
      {
        _characterController.DropLadder();
      }
      _characterController.DropdownFromPlatform();
    }
    else
    {
      input.y = 0;
    }

    if (input.x != 0)
    {
      scale.x = Mathf.Abs(scale.x) * Mathf.Sign(input.x);
    }
    Graphics.transform.localScale = scale;

    return input;
  }
}
