﻿using UnityEngine;
using System.Collections;

public class ParralaxLayer : MonoBehaviour
{
    public float ParralaxAmountX = 1.25f;
    public float ParralaxAmountY = 1.25f;

    private Transform _target;
    private Vector2 _startPosition;

	// Use this for initialization
	void Start ()
	{
	    _target = Camera.main.transform;
	    _startPosition = transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        transform.position = new Vector3(0,0,transform.position.z) + (Vector3)_startPosition - new Vector3(_target.position.x * ParralaxAmountX, _target.position.y * ParralaxAmountY, 0);
	}
}
