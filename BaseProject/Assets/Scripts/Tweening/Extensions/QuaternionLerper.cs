using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Glide
{
    class QuaternionLerper : Lerper
    {
        Quaternion from, to;

        public override void Initialize(object fromValue, object toValue, Behavior behavior)
        {
            from = (Quaternion)fromValue;
            to = (Quaternion)toValue;
        }

        public override object Interpolate(float t, object current, Behavior behavior)
        {
            return Quaternion.Slerp(from, to, t);
        }
    }
}
