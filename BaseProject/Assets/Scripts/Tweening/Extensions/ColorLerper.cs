using UnityEngine;

namespace Glide
{
    class ColorLerper : Lerper
    {
        Color from, to;

        public override void Initialize(object fromValue, object toValue, Behavior behavior)
        {
            from = (Color)fromValue;
            to = (Color)toValue;
        }

        public override object Interpolate(float t, object current, Behavior behavior)
        {
            // better interpolation could be done through HSV color model http://pastebin.com/683Gk9xZ
            return Color.Lerp(from, to, t);
        }
    }
}
