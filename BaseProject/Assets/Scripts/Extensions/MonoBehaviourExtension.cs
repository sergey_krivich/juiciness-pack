﻿using UnityEngine;

namespace MonoBehaviourExtensions
{
  public static class MonoBehaviourExtension
  {
    public static T Create<T>(this T component) where T : MonoBehaviour
    {
      return GameObject.Instantiate(component.gameObject).GetComponent<T>();
    }

    public static T Get<T>(this T component) where T : MonoBehaviour
    {
      var c = component.GetComponent<T>();
      if (c == null)
      {
        Debug.LogFormat("Comp Not found go:{0} comp:{1}", component.gameObject.name, typeof(T).Name);
      }
      return c;
    }
  }
}