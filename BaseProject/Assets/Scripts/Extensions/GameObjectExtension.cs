﻿using UnityEngine;

namespace GameObjectExtensions
{
  public static class GameObjectExtension
  {
    public static GameObject Create(this GameObject gameObject)
    {
      return GameObject.Instantiate(gameObject);
    }

    public static T Create<T>(this GameObject gameObject) where T : MonoBehaviour
    {
      var c = GameObject.Instantiate(gameObject).GetComponent<T>();
      if (c == null)
      {
        Debug.LogFormat("Comp Not found go:{0} comp:{1}", gameObject.name, typeof(T).Name);
      }
      return c;
    }

    public static T Get<T>(this GameObject gameObject) where T : MonoBehaviour
    {
      var c = gameObject.GetComponent<T>();
      if (c == null)
      {
        Debug.LogFormat("Comp Not found go:{0} comp:{1}", gameObject.name, typeof(T).Name);
      }
      return c;
    }
  }
}

