﻿using UnityEngine;

public class JamBehaviour:MonoBehaviour
{
    public Vector3 position 
    {
        get { return transform.position; }
        set { transform.position = value; }
    }

    public Quaternion rotation
    {
        get { return transform.rotation; }
        set { transform.rotation = value; }
    }

    public Vector3 scale
    {
        get { return transform.localScale; }
        set { transform.localScale = value; }
    }


    public float LocalRotation2d
    {
        get { return transform.localEulerAngles.z; }

        set
        {
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, value);
        }
    }
}
