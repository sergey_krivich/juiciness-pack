﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	void Start ()
  {
	    ScreenFade.I.FadeIn();
	}
	
	void Update ()
    {
	    if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
	    {
            ScreenFade.I.FadeOut(0.5f, () =>
            {
                //CHANGE TO YOUR START LEVEL HERE
                SceneManager.LoadScene("scene_1");
            });
	    }
    }
}
