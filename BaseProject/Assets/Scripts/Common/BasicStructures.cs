﻿using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential)]
public struct Point
{
    public int X, Y;

    public Point(int x, int y)
    {
        this.X = x;
        this.Y = y;
    }

    public Point(int value)
    {
        this.X = value;
        this.Y = value;
    }

    public static Point Empty
    {
        get { return new Point(0); }
    }
}

public class Tuple<A, B>
{
    public A a;
    public B b;

    public Tuple(A a, B b)
    {
        this.a = a;
        this.b = b;
    }
}
