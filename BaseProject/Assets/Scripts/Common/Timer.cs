﻿using System;

public class Timer
{
    public bool IsRunning
    {
        get { return _running; }
    }

    public float Elapsed
    {
        get { return _elapsed; }
    }


    /// <summary>
    /// When timer ends its cycle
    /// </summary>
    public EventHandler OnTick;

    public float Duration;

    /// <summary>
    /// Whether timer will repeat its cycle
    /// </summary>
    public bool Looping;

    private bool _running;
    private float _elapsed;


    /// <summary>
    /// Starts timer
    /// </summary>
    /// <param name="reset">indicates that elapsed time should be reset</param>
    public void Run(bool reset = false)
    {
        _running = true;
        if (reset)
        {
            _elapsed = 0.0f;
        }
    }


    /// <summary>
    /// Stops timer, and resets it to 0
    /// </summary>
    public void Reset()
    {
        _elapsed = 0.0f;
        _running = false;
    }

    public void Update(float dt)
    {
        if (!_running)
        {
            return;
        }

        _elapsed += dt;
        if (_elapsed >= Duration)
        {
            _elapsed = 0;
            _running = Looping;
            if (OnTick != null)
            {
                OnTick(this, EventArgs.Empty);
            }
        }
    }
}

